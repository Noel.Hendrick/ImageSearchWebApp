from flask import Flask, render_template, request
import os
import pandas as pd
import re
import nltk
from difflib import SequenceMatcher
from nltk.corpus import stopwords
nltk.download('stopwords')
sw_nltk = stopwords.words('english')

app = Flask(__name__)
picFolder = os.path.join('static', 'pics')
app.config['UPLOAD_FOLDER'] = picFolder

def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()

def MakeDataframe(data):
    df = pd.DataFrame(columns=['ID' 'Text'])
    while len(data) > 5:
        _id = data[2:data.index('<<')].strip()
        data = data[data.index('<<'):]
        _text = data[2:data.index('>>')].strip()
        _text = DataPreprocessing(_text)
        data = data[data.index('>>'):]
        row = [[_id, _text]]
        df2 = pd.DataFrame(row, columns=['ID','Text'])
        df = pd.concat([df,df2])
    return(df)

def DataPreprocessing(text):
    text = text.lower()
    cleantext = re.sub('\W+', ' ', text)
    words = [word for word in cleantext.split(' ') if word.lower() not in sw_nltk]
    words = [x for x in words if not any(c.isdigit() for c in x)]
    new_text = " ".join(words)
    return new_text

def PreformSearch(query, captions):
    relevent = []
    for index, row in captions.iterrows():
        x = similar(query,row['Text'])
        relevent.append((row['ID'], x))
    relevent.sort(key=lambda i: i[1], reverse=True)
    final = []
    for i in relevent:
        final.append(i[0])
    return final


with open('captions.txt', 'r') as file:
    data = file.read().rstrip()
captions = MakeDataframe(data)


@app.route('/')
def home():
    return render_template('home.html')


@app.route('/shortenurl', methods = ['GET','POST'])
def shortenurl():
    shortcode=request.form['shortcode']
    imagesneeded = PreformSearch(shortcode, captions)
    imageList = imagesneeded
    #imageList = os.listdir('static/pics')
    imagelist = ['pics/' + image for image in imageList]
    return render_template('shortenurl.html', shortcode=request.form['shortcode'], imagelist=imagelist)

@app.route("/images")
def index():
    imageList = os.listdir('static/pics')
    imagelist = ['pics/' + image for image in imageList]
    return render_template("images.html", imagelist=imagelist)

app.run(host='localhost', port=5000)